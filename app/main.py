from flask import Flask

app = Flask(__name__)

@app.route('/')
def index():
    return 'Welcome to krikri 2.0!'

app.run(debug=True, host='0.0.0.0', port=80)