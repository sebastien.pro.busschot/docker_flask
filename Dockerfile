FROM ubuntu
WORKDIR /app
COPY flask /usr/bin/flask
COPY flask.service.ini /etc/systemd/system/flask.service
COPY app /app
WORKDIR /app
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y python python3-pip systemctl && \
    pip install -r requirements.txt && \
    systemctl enable flask.service
EXPOSE 80
CMD [ "systemctl", "start", "flask" ]